/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
module.exports = callback5;

const callback2 = require('./callback2');
const callback3 = require('./callback3');

function callback5(boardData, listData, cardData, input) {
    setTimeout(() => {
        /* find details for a given board name */
        let boardDetails = boardData.find(board => board.name === input.board); 
        console.log(boardDetails); // Output

        let boardId = boardDetails.id; 
        
        /* find lists for a board Id */
        callback2(listData, boardId, (err, data) => {
            if(err !== null) {
                console.log(err); 
            }
            else {
                let listsOfBoard = data; 
                console.log(listsOfBoard);  // Output
            }
        });

        /* find cards for the both list Id */
        input.list.forEach(inputList => {
            let listId = null; 
            for(let board in listData) {
                for(let list of listData[board]) {
                    if(list.name === inputList) {
                        listId = list.id; 
                    }
                }
            }

            callback3(cardData, listId, (err, data) => {
                if(err !== null) {
                    console.log(err); 
                }
                else {
                    let cardsOfList = data; 
                    console.log(cardsOfList); // Output
                }
            });
        });
    }, 2 * 1000);
}

