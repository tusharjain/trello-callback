/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

module.exports = callback6;

const callback2 = require('./callback2');
const callback3 = require('./callback3');

function callback6(boardData, listData, cardData, input) {
    setTimeout(() => {
        /* find details for a given board name */
        let boardDetails = boardData.find(board => board.name === input.board); 
        console.log(boardDetails); // Output

        let boardId = boardDetails.id; 
        
        /* find lists for a board Id */
        callback2(listData, boardId, (err, data) => {
            if(err !== null) {
                console.log(err); 
            }
            else {
                let listsOfBoard = data; 
                console.log(listsOfBoard);  // Output
            }
        });

        /* find cards for all list IDs */
        Object.keys(cardData)
              .forEach(listId => {
                callback3(cardData, listId, (err, data) => {
                    if(err !== null) {
                        console.log(err); 
                    }
                    else {
                        let cardsOfList = data; 
                        console.log(cardsOfList); // Output
                    }
                });
              });

    }, 2 * 1000);
}

