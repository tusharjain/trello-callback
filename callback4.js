/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const callback2 = require('./callback2');
const callback3 = require('./callback3');


function callback4(boardData, listData, cardData, input) { // input.board input.list
    setTimeout(() => {
        let boardDetails = boardData.find(board => board.name === input.board); 
        console.log(boardDetails); // Board details 

        let boardId = boardDetails.id; 
        
        callback2(listData, boardId, (err, data) => {
            if(err !== null) {
                console.log(err); 
            }
            else {
                let listsOfBoard = data; 
                console.log(listsOfBoard);  // All the lists of a boardId
            }
        });

        let listId = null; 
        for(let board in listData) {
            for(let list of listData[board]) {
                if(list.name === input.list) {
                    listId = list.id; 
                }
            }
        }

        callback3(cardData, listId, (err, data) => {
            if(err !== null) {
                console.log(err); 
            }
            else {
                let cardsOfList = data; 
                console.log(cardsOfList); // All the cards of a listId 
            }
        });

    }, 2000);
}

module.exports = callback4;