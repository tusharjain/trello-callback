const callback5 = require("../callback5");

const BOARDS = require("../data/boards.json");
const LISTS = require("../data/lists.json");
const CARDS = require("../data/cards.json");

let input = {
  board: "Thanos",
  list: ["Mind", "Space"]
};

callback5(BOARDS, LISTS, CARDS, input);
