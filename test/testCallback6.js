const callback6 = require("../callback6");

const BOARDS = require("../data/boards.json");
const LISTS = require("../data/lists.json");
const CARDS = require("../data/cards.json");

let input = {
  board: "Thanos",
};

callback6(BOARDS, LISTS, CARDS, input);
