const callback4 = require("../callback4");
const BOARDS = require("../data/boards.json");
const LISTS = require("../data/lists.json");
const CARDS = require("../data/cards.json");

let input = {
  board: "Thanos",
  list: "Mind",
};

callback4(BOARDS, LISTS, CARDS, input);
